# Controller Template by BOTFRIENDS

<img src="https://botfriends.de/wp-content/uploads/2018/01/BOTfriends_logo-01_m.png" alt="drawing" width="300"/>

Example of how to build a Chatbot Controller with DialogFlow and Facebook Messenger

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them:
* nodeJs 8 or higher (download here: https://nodejs.org/en/download/)

### Installing

After cloning the projects you need to do the following

```
npm install
```

Now you have installed all the dependencies you need for this project.

### Run the Development Server

```
npm run dev
````

An express server for development will be started with nodemon

### Add config.js file
To be able to run the server you need to create a config.js file in the root directory. This should look like the following
```
module.exports = {
  FB_PAGE_TOKEN: process.env.FB_PAGE_TOKEN || '',
  FB_VERIFY_TOKEN: process.env.FB_VERIFY_TOKEN || '',
  FB_APP_SECRET: process.env.FB_APP_SECRET || '',
  GOOGLE_PROJECT_ID: process.env.GOOGLE_PROJECT_ID || '',
  DF_LANGUAGE_CODE: process.env.DF_LANGUAGE_CODE || '',
  GOOGLE_CLIENT_EMAIL: process.env.GOOGLE_CLIENT_EMAIL || '',
  GOOGLE_PRIVATE_KEY: process.env.GOOGLE_PRIVATE_KEY || '',
  WEATHER_API_KEY: process.env.WEATHER_API_KEY ||'',
  TRANSLATION_API_KEY: process.env.TRANSLATION_API_KEY || '',
  CMC_API_KEY: process.env.CMC_API_KEY || ''
}
```

Either you define a .env file with the environment variables shown above or you add the required strings to config.js.

# Challenges
### Challenge 1
In Challenge one you need to integrate any API of your choice. Hints and TODOs can be found in
```
services/dialogFlowService.js
```

Also we already created some functions with empty bodies you can use. These can be found in
```
external/
```

### Challenge 2
For challange two the goal is to use any Language Detection API plus a Translation API to detect if the language is not english

Hints are in the following files

```
services/dialogFlowService.js
services/messageReceiver.js
```

### Challenge 3
Now you have to shine.
Use the storage functions we created to let the user setup a nice userprofile :tada:


## Built With

* [Express](http://expressjs.com/) - The web framework used

## Authors

* **Simon Kaiser** - *Initial work* - [BOTfriends](https://botfriends.de)
